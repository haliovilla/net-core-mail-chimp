﻿using MailChimp.Net;
using MailChimp.Net.Core;
using MailChimp.Net.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MailChimpConxole
{
    public class EmailSender
    {
        private static string apiKey = "your_api_key";
        private static string ListId = "list_id";
        private static int TemplateId = 123456;

        private static MailChimpManager _mailChimpManager = new MailChimpManager(apiKey);
        private static Setting _campaignSetting = new Setting
        {
            ReplyTo = "haliovilla@hotmail.com",
            FromName = "TI Plus Morelia",
            Title = "Mail Chimp Test",
            SubjectLine = ".Net Core Mail Chimp integration"
        };

        public static List<Template> GetAllTemplates()
        {
            var templates = _mailChimpManager.Templates.GetAllAsync().Result.ToList();
            foreach (var item in templates)
            {
                Console.WriteLine($"{item.Id}\t{item.Name}");
            }
            return templates;
        }

        public static List<List> GetAllMailingLists()
        {
            var lists= _mailChimpManager.Lists.GetAllAsync().Result.ToList();
            foreach (var item in lists)
            {
                Console.WriteLine($"{item.Id}\t{item.Name}");
            }
            return lists;
        }

        public static Content GetTemplateDefaultContent(string templateId = "")
        {
            if (String.IsNullOrEmpty(templateId))
                templateId = TemplateId.ToString();

            var result = (Content)_mailChimpManager.Templates.GetDefaultContentAsync(templateId).Result;
            //Console.WriteLine(result); ;
            return result;
        }

        public static void CreateAndSendCampaign(string html)
        {
            var campaign = _mailChimpManager.Campaigns.AddAsync(new Campaign
            {
                Settings = _campaignSetting,
                Recipients = new Recipient { ListId = ListId },
                Type = CampaignType.Regular
            }).Result;
            var timeStr = DateTime.Now.ToString();
            var content = _mailChimpManager.Content.AddOrUpdateAsync(
             campaign.Id,
             new ContentRequest()
             {
                 Template = new ContentTemplate
                 {
                     Id = TemplateId,
                     Sections = new Dictionary<string, object>()
                {
       { "body_content", html },
       { "preheader_leftcol_content", $"<p>{timeStr}</p>" }
                }
                 }
             }).Result;
            _mailChimpManager.Campaigns.SendAsync(campaign.Id).Wait();
        }

        public static async Task GetListMembers()
        {
            var listMembers = await _mailChimpManager.Members.GetAllAsync(ListId).ConfigureAwait(false);
            foreach (var item in listMembers)
            {
                Console.WriteLine(item.EmailAddress);
            }
        }

        public static async Task AddEmailToList(string emailAddress, string firstName, string lastName, string listId = "")
        {
            if (String.IsNullOrEmpty(listId))
                listId = ListId;

            var member = new Member 
            { 
                EmailAddress = emailAddress,
                StatusIfNew = Status.Subscribed
            };
            member.MergeFields.Add("FNAME", firstName);
            member.MergeFields.Add("LNAME", lastName);
            await _mailChimpManager.Members.AddOrUpdateAsync(listId, member);

        }



    }
}
